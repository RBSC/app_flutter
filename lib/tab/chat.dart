import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app_ok/imagen.dart';

class Chat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Container contactList(
        String name, String time, String message, Image image) {
      return Container(
        alignment: Alignment.topCenter,
        padding: EdgeInsets.only(
            top: 0,
          right: 0
        ),
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(
          horizontal: 0
        ),
        child: Card(
         child: Wrap(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(
                      vertical: 1,
                           horizontal:10
                  ),
                  child: Text(
                      name,
                    style: TextStyle(
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 1,
                    horizontal: 10
                  ),
                  child: Text(
                      time,
                    style: TextStyle(
                   fontStyle: FontStyle.italic
                  )
                  ),
                ),
                Align(
                  alignment: Alignment(-0.8,0),
                  child: FlatButton.icon(
                      padding: EdgeInsets.all(7),
                      onPressed: ()=>{},
                      icon: Icon(
                        Icons.near_me,
                        size: 16,
                          color: Colors.grey
                      ),
                      color: Colors.white,
                      label: Text(
                        message,
                        style: TextStyle(
                           fontStyle: FontStyle.italic,
                          color: Colors.grey
                        ),
                      )
                  ),
                )
              ],
            ),
            Align(
              alignment: Alignment(1.1,0),
              widthFactor: 0,
              child: CircleAvatar(
                child: image,
                maxRadius: 45,
                minRadius: 45,
                backgroundColor: Colors.white,
              ),
            )
          ],
        ),
        )
      );
    }

    return Scaffold(
        body: Stack(
            children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: 60,
                  horizontal: 25
                ),
                child: Text(
                  "Your Chat's",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                     color: Colors.white
                  ),
                ),
                height: 200,
                width: 800,
                color: Colors.blue,
              )
            ],
          ),
          Container(
            padding: EdgeInsets.only(
              top: 100,
              right: 1,
              left: 1,
             ),
            child: Center(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      scrollDirection: Axis.vertical,
                      children: <Widget>[
                        contactList("Jim Doe", "seen 2 mins ago",
                            "Hey want to catch up today?", derechaC),
                        contactList(
                            "Jane Doe",
                            "online",
                            "Ohh that's cool. Would love to\n" "meet you! Dinner?",
                            centroC),
                        contactList(
                            "Ek aour Doe",
                            "online",
                            "Bhai milie time pass hojayega\n" "dono ka",
                            izquierdaC),
                        contactList(
                            "John Doe",
                            "seen 2 mins ago",
                            "I cannot believe this! This is \n" "ridiculous!",
                            derechaC),
                        contactList("Ye b Doe", "online",
                            "Tu mat mil bhai mood nai\n" "bigadna mike",
                            centroC),
                        contactList("Raul Doe", "seen 15 mins ago",
                            "Hey want to catch up today?", izquierdaC),
                        contactList("Kelly Doe", "seen 10 mins ago",
                            "Hey want to catch up today?", centroC),
                        contactList("King Doe", "online",
                            "Hey want to catch up today?", izquierdaC)
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ]));
  }
}