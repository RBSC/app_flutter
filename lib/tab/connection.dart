import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_ok/imagen.dart';

class Connection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Container connectList(String name,String state, Image image){
      return Container(
        alignment: Alignment.topCenter,
        padding: EdgeInsets.only(
          top: 0,
          right: 0
        ),
         width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(
          horizontal: 0
        ),
        child: Card(
           child: Wrap(
             children: <Widget>[
               Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                     Container(
                       padding: EdgeInsets.symmetric(
                         vertical: 1,
                          horizontal: 10
                       ),
                       child: Text(
                         name,
                          style: TextStyle(
                            fontWeight: FontWeight.bold
                          ),
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.symmetric(
                         vertical: 3,
                         horizontal: 10
                       ),
                       child: Text(
                         state,
                         style: TextStyle(
                           fontStyle: FontStyle.italic
                         ),
                       ),
                     ),

                      Align(
                        alignment: Alignment(-0.8,0),
                        child: FlatButton.icon(
                          padding: EdgeInsets.all(7),
                          icon: Icon(
                            Icons.close,
                            size: 16,
                            color: Colors.white,
                          ),
                          onPressed: ()=>{},
                          color: Colors.blue,
                          label: Text(
                            'Remove',
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.white
                            ),
                          ),
                        ),
                      )
                   ],
               ),
               Align(
                 alignment: Alignment(1.1,0),
                  widthFactor: 0,
                 child: CircleAvatar(
                   child: image,
                   maxRadius: 45,
                   minRadius: 45,
                   backgroundColor: Colors.white,
                 )
               )
             ],
           ),
        ),
      );
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 25,
                     vertical: 60
                ),
                child: Text(
                  'Your Connections',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 20,
                     color: Colors.white,
                    fontWeight: FontWeight.bold
                  ),
                ),
                height: 200,
                 width: 800,
                color: Colors.blue,
              )
            ],
          ),
          Container(
            padding: EdgeInsets.only(
              top: 100,
              right: 1,
              left: 1
            ),
             child: Center(
               child: Column(
                 children: <Widget>[
                   Expanded(
                     child: ListView(
                        scrollDirection: Axis.vertical,
                       children: <Widget>[
                          connectList('Jim Doe','Enjoying life and living with love.',derechaC ),
                         connectList('Jane Doe','Enjoying life and living with love.',centroC ),
                         connectList('John Doe','Enjoying life and living with love.',izquierdaC ),
                         connectList('Lucero Doe','Enjoying life and living with love.',centroC ),
                         connectList('Messi Doe','Enjoying life and living with love.',izquierdaC ),
                         connectList('Karol Doe','Enjoying life and living with love.',centroC ),
                         connectList('Pilar Doe','Enjoying life and living with love.',derechaC )
                       ],
                     ),
                   )
                 ],
               ),
             ),
          )
        ],
      )
    );
  }
}
