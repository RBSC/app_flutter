import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Center(
              child:  Stack(
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.blue
                    ),
                    height: 200,
                    padding: EdgeInsets.only(
                       bottom: 60
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: CircleAvatar(
                            backgroundImage: ExactAssetImage(
                              'img/Image_Container2.png',
                              scale: 12
                            ),
                            maxRadius: 45,
                            minRadius: 45,
                          ),
                        ),
                        Text(
                          'John Doe',
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.white
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Align(
                      alignment: Alignment(0,-0.3),
                      child: Container(
                        height: 80,
                        width: 300,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0,1)
                              )
                            ]
                        ),
                        child: Row(
                          children: <Widget>[
                            MaterialButton(
                              minWidth: (MediaQuery.of(context).size.width - 100) / 2,
                              child: Column(
                                children: <Widget>[
                                  FlatButton.icon(
                                    onPressed: ()=>{},
                                    icon: Icon(
                                        Icons.favorite_border
                                    ),
                                    label: Text(
                                      '30',
                                      style: TextStyle(
                                          fontSize: 25
                                      ),
                                    ),
                                  ),
                                  Text(
                                      'Connections'
                                  )
                                ],
                              ),
                            ),
                            MaterialButton(
                              minWidth: (MediaQuery.of(context).size.width -20) / 2,
                              child: Column(
                                children: <Widget>[
                                  FlatButton.icon(
                                    onPressed: ()=>{},
                                    icon: Icon(
                                        Icons.chat_bubble_outline
                                    ),
                                    label: Text(
                                      '10',
                                      style: TextStyle(
                                          fontSize: 25
                                      ),
                                    ),
                                  ),
                                  Text(
                                      'Chats'
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 220,
                      right: 20,
                      left: 10,
                    ),
                    child: Column(
                       children: <Widget>[
                         Expanded(
                           child: ListView(
                             scrollDirection: Axis.vertical,
                             children: <Widget>[
                               Container(
                                 padding: EdgeInsetsDirectional.only(
                                     start: 40
                                 ),
                                 child: Column(
                                   children: <Widget>[
                                     Align(
                                       alignment: Alignment(-1,0),
                                       child: Padding(
                                         padding: EdgeInsets.only(
                                             top:20
                                         ),
                                         child: Text(
                                           'Status',
                                           style: TextStyle(

                                           ),
                                         ),
                                       ),
                                     ),
                                     TextField(
                                       decoration: InputDecoration(
                                         hintText: 'Sab Jaana Jaruri Hai?',
                                       ),
                                     ),

                                     Align(
                                       alignment: Alignment(-1,0),
                                       child: Padding(
                                         padding: EdgeInsets.only(
                                             top:20
                                         ),
                                         child: Text(
                                             'Email'
                                         ),
                                       ),
                                     ),
                                     TextField(
                                       decoration: InputDecoration(
                                         hintText: 'maibdoehai@doerox.com',
                                       ),
                                     ),
                                     Align(
                                       alignment: Alignment(-1,0),
                                       child: Padding(
                                         padding: EdgeInsets.only(
                                             top:20
                                         ),
                                         child: Text(
                                             'Phone Numbre'
                                         ),
                                       ),
                                     ),
                                     TextField(
                                       decoration: InputDecoration(
                                         hintText: '+91 9123456789',
                                       ),
                                     ),
                                     Align(
                                       alignment: Alignment(-1,0),
                                       child: Padding(
                                         padding: EdgeInsets.only(
                                             top:20
                                         ),
                                         child: Text(
                                             'Date of birth'
                                         ),
                                       ),
                                     ),
                                     TextField(
                                       decoration: InputDecoration(
                                         hintText: '31st February, 1989',
                                       ),
                                     ),
                                   ],
                                 ),
                               ),
                               Align(
                                 alignment: Alignment(1,0),
                                 heightFactor: 1.3,
                                 child: Container(
                                   height: 50,
                                   width: 295,
                                   decoration: BoxDecoration(
                                     borderRadius: BorderRadius.only(
                                         bottomLeft: Radius.circular(10),
                                         topLeft: Radius.circular(10),
                                         topRight: Radius.circular(10),
                                         bottomRight: Radius.circular(10)
                                     ),
                                     color: Colors.red,
                                   ),
                                   child: MaterialButton(
                                     onPressed: ()=>{},
                                     child: Text(
                                       'Log out',
                                       style: TextStyle(
                                           color: Colors.white,
                                           fontSize: 25
                                       ),
                                     ),
                                   ),
                                 ),
                               )
                             ],
                           ),
                         )
                       ],
                    ),
                  )
                    ],
              ),
          ),
      )
    );
  }
}
