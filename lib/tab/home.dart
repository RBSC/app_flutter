import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ok/imagen.dart';


class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Container cards(Image image, String title) {
      return Container(
        alignment: Alignment.topCenter,
        padding:EdgeInsets.only(
          top: 20,
              left:20
        ),
        width: MediaQuery.of(context).size.width -50 ,
        margin: EdgeInsets.symmetric(
          horizontal: 0
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(0),
              topLeft: Radius.circular(0),
              bottomLeft: Radius.circular(0),
              bottomRight: Radius.circular(0)
          )
        ),
        child: Card(
          child: Wrap(
            children: <Widget>[
              Container(
                child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)
                    ),
                    child: image
                ),
              ),
              ListTile(
                title: Text(
                  title,
                  style: new TextStyle(
                      fontSize: 14
                  ),
                ),
                subtitle: Text(
                      "25 \n\n"
                      "This is some description about the person"
                          "what he she expects from the partner and"
                          "also what they want to achieve  the life.\n\n"
                      "10 mins away",
                      style: TextStyle(
                        fontSize: 9,
                      )
                )
              ),
              SizedBox(
                child: Row(
                  children: <Widget>[
                    MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10)
                          ),
                      ),
                      height: 50,
                      minWidth: (MediaQuery.of(context).size.width - 100) / 2,
                      color: Colors.red,
                      child: FlatButton.icon(
                          icon: Icon(
                              Icons.favorite_border,
                            color: Colors.white,
                          ),
                        label:Text(
                            "Connect",
                          style: TextStyle(
                            fontSize: 12.5,
                            color: Colors.white
                          ) ,
                        ) ,
                      ),
                      onPressed: ()=>{},
                      splashColor: Colors.redAccent,
                    ),
                    MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(10)
                        ),
                      ),
                      height: 50,
                      minWidth: (MediaQuery.of(context).size.width - 90) / 2,
                      color: Colors.green,
                      child: FlatButton.icon(
                        icon: Icon(
                          Icons.near_me,
                          color: Colors.white,
                        ),
                        label:Text(
                          "Message",
                          style: TextStyle(
                              fontSize: 12.3,
                              color: Colors.white
                          ) ,
                        ) ,
                      ),
                      onPressed: ()=>{},
                      splashColor: Colors.greenAccent,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
          children: <Widget>[
            Container(
              height: 200,
              color: Colors.blue,
              padding: EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 60
              ),
            )
          ],
          ),
           Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        cards(derecha, "Jim Doe"),
                        cards(centro, "Jane Doe"),
                        cards(izquierda, "Jhon Doe")
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),

    );
  }
}
