import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ok/tab/chat.dart';
import 'package:flutter_app_ok/tab/home.dart';
import 'package:flutter_app_ok/tab/connection.dart';
import 'package:flutter_app_ok/tab/profile.dart';


class Inicio extends StatefulWidget {
  @override
  Menu createState() => Menu();
}

class Menu extends State<Inicio> with SingleTickerProviderStateMixin {
  TabController controlador;

  @override
  void initState() {
    super.initState();
    controlador = TabController(
        length: 4,
        vsync: this);
  }

  @override
  void dispose() {
    controlador.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        children: <Widget>[
          Home(),
          Connection(),
          Chat(),
          Profile()
        ],
        controller: controlador,
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.symmetric(
            vertical: 12,
            horizontal: 8
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomRight: Radius.circular(20)
          ),
        ),
        child: TabBar(
          indicatorWeight: 0.1,
          labelColor: Colors.purple,
          unselectedLabelColor: Colors.grey,
          indicatorColor: Colors.purple,

          tabs: <Tab>[
            Tab(
              text: "Home",
              icon: Icon(Icons.dashboard),
            ),
            Tab(
              text: "Connect",
              icon: Icon(Icons.favorite_border),
            ),
            Tab(
              text: "Chats",
              icon: Icon(Icons.chat_bubble_outline),
            ),
            Tab(
              text: "Profile",
              icon: Icon(Icons.person_outline),
            )
          ],
          controller: controlador,
        ),
      ),
    );
  }
}

